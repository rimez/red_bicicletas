// var  map = L.map(main_map).setView([-34.6012424,-58.3861497], 13);
// L.tileLayer('https://{s}.title.openstreetmap.org/{z}/{x}/{y}.png', {
//     attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
// }).addTo(map);

// -26.883777345611694, -56.853604733058276

var mymap = L.map('main_map').setView([-27.332353,-55.8640557], 15);
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1Ijoicm1lemE5OTAiLCJhIjoiY2tqeHFqcmZnMDZqaDJ2cXdrdGxjMzNkdyJ9.i95AUx3UPboO3aPAaya4iw'
}).addTo(mymap);

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function (result){
        console.log(result);
        result.bicicletas.forEach(function (bici){
            L.marker(bici.ubicacion, {title: bici.id}).addTo(mymap);
        });
    }
})